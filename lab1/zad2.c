#include <stdio.h>
#include <stdlib.h>

int main()
{
    int hh, mm, ss;
    char sep;
    printf ("Vvedite tekuschee vremja: hh:mm:ss\n");
        scanf ("%d%c%d%c%d", &hh, &sep, &mm, &sep, &ss);
    if (hh<0 || hh>=24 || mm<0 || mm>=60 || ss<0 || ss>=60)
        printf ("Oshibka! Takogo vremeni ne suschestvuet\n");
    else if (hh >= 06 && hh < 12)
        printf ("Dobroe utro!\n");
    else if (hh >= 12 && hh < 17)
        printf ("Dobriy den'!\n");
    else if (hh >= 17 && hh < 23)
        printf ("Dobriy vecher!\n");
    else
        printf ("Dobroy nochi!\n");
    return 0;
}